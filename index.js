const http = require('http');
const url = require('url');
const StringDecoder = require('string_decoder').StringDecoder;
const config = require('./config');
const router = require('./router');

const httpServer = http.createServer((request, response) => handleConnection(request, response));

httpServer.listen(config.httpPort, () => console.log(`Listening on port ${config.httpPort}!`));

const handleConnection = (request, response) => {
    const parsedUrl = url.parse(request.url, true);
    const pathname = parsedUrl.pathname;
    const trimmedPath = pathname.replace(/^\/+|\/+$/g, '');
    const queryStringObject = parsedUrl.query;
    const method = request.method.toLowerCase();
    const headers = request.headers;

    const decoder = new StringDecoder('utf-8');
    let payload = '';
    request.on('data', data => payload += decoder.write(data));
    request.on('end', () => {
        payload += decoder.end();

        const chosenRoute = typeof (router[trimmedPath]) !== 'undefined'
            ? router[trimmedPath]
            : router.notFound;

        const data = {
            trimmedPath,
            queryStringObject,
            method,
            headers,
            payload
        };

        chosenRoute(data, (statusCodeValue, payload) => {
            const statusCode = typeof (statusCodeValue) == 'number' ? statusCodeValue : 200;
            const payloadData = typeof (payload) == 'object' ? payload : {};
            const payloadString = JSON.stringify(payloadData);

            response.setHeader('Content-Type', 'application/json');
            response.writeHead(statusCode);
            response.end(payloadString);

            console.log(`Response returned: STATUS: ${statusCode}, PAYLOAD: ${payloadString}`);
        });
    })
}

