const handlers = {};

handlers.notFound = function(data, callback) {
    callback(404, notFoundMessage);
}

handlers.hello = function(data, callback) {
    data.method === 'post'
        ? callback(200, helloMessage)
        : callback(406, wrongMethodMessage);
}

const helloMessage = { "greeting": "Welcome to the first assignment!" };

const notFoundMessage = { "sorry": "Requested address doesn\'t exist."};

const wrongMethodMessage = { "method": "Use POST method to see greeting message."};


module.exports = handlers;